# -*- coding: utf-8 -*-
from flask_restful import Resource
from flask import request, g
from tools import auth


class OdooResource(Resource):

    odoo_model = False
    odoo_fields = []

    def dispatch_request(self, *args, **kwargs):
        self.method_decorators.append(auth.login_required)
        return super(OdooResource, self).dispatch_request(*args, **kwargs)

    def process_get(self, odoo, result):
        """ Process Get Result
        To inherit in final resources"""
        return result

    def get(self, res_id=False):
        if not self.odoo_model:
            return {'Warning!': 'Model or Fields Undefined'}
        arguments = request.args
        if res_id:
            domain = [('id', '=', res_id)]
        elif 'domain' in arguments:
            domain = eval(request.args['domain'])
            # TODO Test its formed ok
        else:
            domain = []
        if 'offset' in arguments:
            offset = eval(request.args['offset'])
        else:
            offset = 0
        if 'limit' in arguments:
            limit = eval(request.args['limit'])
        else:
            limit = None
        if 'order' in arguments:
            order = eval(request.args['order'])
        else:
            order = None
        odoo = g.user.get_connection()
        context = odoo.env.context
        if 'context' in arguments:
            context.update(eval(request.args['context']))
        proxy = odoo.env[self.odoo_model]
        result = proxy.search(
            domain,
            offset=offset,
            limit=limit,
            order=order,
            context=context)
        if 'fields' in arguments:
            fields = request.args['fields']
            fields = (fields == 'all') and self.odoo_fields or eval(fields)
            result = odoo.execute(self.odoo_model,
                                  'read',
                                  result,
                                  fields)
            result = self.process_get(odoo, result)
        return result

    def post(self):
        arguments = request.get_json(force=True)
        odoo = g.user.get_connection()
        proxy = odoo.env[self.odoo_model]
        res_id = proxy.create(arguments)
        return {'id': res_id}, 201

    def put(self, res_id):
        arguments = request.get_json(force=True)
        odoo = g.user.get_connection()
        proxy = odoo.env[self.odoo_model]
        element = proxy.browse(int(res_id))
        res = element.write(arguments)
        return {'result': res}

    def delete(self, res_id):
        odoo = g.user.get_connection()
        proxy = odoo.env[self.odoo_model]
        element = proxy.browse(int(res_id))
        res = element.unlink()
        return {'result': res}
