# -*- coding: utf-8 -*-
import odoorpc


class OdooUser:

    def __init__(self, username, password, odoo_server):
        self.username = username
        self.password = password
        self.odoo_server = odoo_server

    def get_connection(self):
        try:
            odoo = odoorpc.ODOO(
                self.odoo_server.server,
                port=self.odoo_server.port)
            odoo.login(
                self.odoo_server.database,
                self.username,
                self.password)
            return odoo
        except:
            return False


class OdooServer:

    def __init__(self, server, port, database):
        self.server = server
        self.port = port
        self.database = database

    def login(self, username, password):
        try:
            odoo = odoorpc.ODOO(self.server, port=self.port)
            odoo.login(self.database, username, password)
        except:
            return False
        return OdooUser(username, password, self)
